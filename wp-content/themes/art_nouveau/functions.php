<?php
$themename = "art_nouveau";
$shortname = "art_nouveau";

$categories = get_categories('hide_empty=0&order_by=name');
$wp_cats = array();

foreach ($categories as $category_list) {
    $wp_cats[$category_list->cat_ID] = $category_list->cat_name;
}

function art_nouveau_content_nav( $html_id ) {
    global $wp_query;

    if ( $wp_query->max_num_pages > 1 ) : ?>
        <nav id="<?php echo esc_attr( $html_id ); ?>">
            <div class="nav-previous"><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> Older posts', 'impressionism' ) ); ?></div>
            <div class="nav-next"><?php previous_posts_link( __( 'Newer posts <span class="meta-nav">&rarr;</span>', 'impressionism' ) ); ?></div>
        </nav><!-- #nav-above -->
    <?php endif;
}


function art_nouveau_add_init()
{
    global $themename;
    $file_dir = get_bloginfo('template_directory');
    wp_enqueue_style("functions", $file_dir . "/functions/css/functions.css", false, "1.0", "all");
    wp_enqueue_style("{$themename}", $file_dir . "/functions/css/jquery-ui.css", false, "1.0", "all");
    wp_enqueue_script("{$themename}_js", $file_dir . "/functions/js/script.js", false, "1.0");
}

if (get_option($themename.'_theme_options_menu')) {
    $theme_options_menu = get_option($themename.'_theme_options_menu');
} else {
    add_option($themename.'_theme_options_menu', array(
        $themename.'_menu_title' => array(
            array(
                'title' => 'About',
                'link' => '#about'
            ),
            array(
                'title' => 'Portfolio',
                'link' => '#portfolio'
            ),
            array(
                'title' => 'Blog',
                'link' => '#blog'
            ),
            array(
                'title' => 'Contact',
                'link' => '#contact'
            ),
        ),
        $themename.'_header_site_name' => 'Lorem Ipsum',
        $themename.'_header_site_slogan' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
        $themename.'_portfolio_block_title' => 'Portfolio',
        $themename.'_about_block_title' => 'About Us',
        $themename.'_our_team_block_title' => array(
            'title' => 'Our Team',
            'url' => get_bloginfo('template_directory') . '/img/our-team.png',
            'description' => '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum itaque non pariatur quisquam sapiente. Accusantium ea error, harum libero obcaecati odio veritatis! Beatae cupiditate deserunt et, laboriosam minima recusandae tempora.</p>',
            'float' => 'left',
        ),
        $themename.'_blog_block' => array(
            'title' => 'Blog',
            'photo_float' => 'left',
            'default_image' => 'on',
        ),
        $themename.'_contact_us_block' => array(
            'title' => 'Contact Us',
            'address' => '<p>325 Sharon Park Drive, Suite #756 <br/>Menlo Park, CA 94025</p><p>+(1) 745-23-23</p><p>email@adress.com</p>',
        ),
        $themename.'_social_network_custom' => array(
            'facebook' => '',
            'flickr' => '',
            'myspace' => '',
            'linkedin' => '',
            'twitter' => '',
            'vimeo' => '',
            'tumblr' => '',
        ),

    ));
    $theme_options_menu = get_option($themename.'_theme_options_menu');
}


function art_nouveau_page_add()
{
    add_submenu_page('themes.php', 'My Theme Options', 'Theme Options', 8, 'themeoptions', 'theme_page_options');
}

function catch_that_image($content)
{

    $first_img = array();

    preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $content, $matches);
    $first_img['image'] = $matches [1] [0];
    $first_img['text'] = '<p>' . str_replace($matches [0] [0], '', $content) . '</p>';

    if (empty($first_img['image'])) {
        $first_img['image'] = get_bloginfo('template_directory') . "/img/default.png";
        $first_img['default'] = true;
    }

    return $first_img;
}


function getCategoryPostList($postId)
{
    $categoryId = wp_get_post_categories($postId);
    $categoryList = '';
    foreach ($categoryId as $val) {
        $categoryList .= $val != 1
            ? '<a href="' . get_category_link($val) . '">' . get_the_category_by_ID($val) . '</a>, '
            : '';
    }

    return count($categoryId) == 1 && $categoryId[0] == 1
        ? '&nbsp;'
        : 'Category: ' . rtrim($categoryList, ', ');
}

function art_nouveau_comment($comment, $args, $depth) {
$GLOBALS['comment'] = $comment;

$commentId = get_comment_ID();
$commentClass = get_comment_class($commentId);
$commentEdit = get_edit_comment_link($commentId);
$commentAuthor = get_comment_author($commentId);
$commentText = get_comment_text($commentId);
$commentDate = get_comment_date('d.m.Y', $commentId);
$commentReplyLink = get_comment_reply_link(array_merge($args, array('reply_text' => __('Reply'), 'depth' => $depth, 'max_depth' => $args['max_depth'])));
?>
<li <?php echo $commentClass; ?> id="li-comment-<?php echo $commentId; ?>">
<impressionismicle id="comment-<?php comment_ID(); ?>" class="comment">
    <div class="comment-container">
        <div class="comment comment-header">
            <div class="comment-author">
                <?php echo $commentAuthor;?>:
            </div>
            <div class="comment-date">
                <?php echo $commentDate;?>
            </div>
        </div>
        <div class="comment comment-content">
            <div class="comment-text">
                <?php echo $commentText;?>
            </div>
        </div>
        <div class="comment comment-footer">
                <span class="comment-reply">
                    <?php echo $commentReplyLink;?>
                </span>
                <span class="comment-edit">
                    <?php if(current_user_can('manage_options')):?>
                        <a href="<?php echo $commentEdit;?>">Edit</a>
                    <?php endif;?>
                </span>
        </div>

    </div>
    <!-- .reply -->
</impressionismicle>
<!-- #comment-## -->

<?php
}


add_action('wp_ajax_nopriv_get_post_by_category', 'getPostByCategory');
add_action('wp_ajax_get_post_by_category', 'getPostByCategory');
function getPostByCategory()
{

    $categoryId = htmlspecialchars($_POST['categoryId']);
    $categoryText = htmlspecialchars($_POST['categoryText']);
    $postByCategory = array();
    $args = array('category' => $categoryId);
    $postByCategoryAll = $categoryId != 0 ? get_posts($args) : get_posts();
    foreach ($postByCategoryAll as $key => $val) {
        $postByCategory[$key]['category_id_custom'] = getCategoryPostList($val->ID);
        $postContent = catch_that_image($val->post_content);
        $shortText = explode('<!--more-->', $postContent['text']);
        $postByCategory[$key]['shortText'] = '<p>' . trim($shortText[0] . ' <a href="' . $val->guid . '" class="more-link">more</a></p>');
        $postByCategory[$key]['image'] = $postContent['image'];
        $postByCategory[$key]['id'] = $val->ID;
        $postByCategory[$key]['comment_count'] = $val->comment_count;
        $postByCategory[$key]['guid'] = $val->guid;
        $postByCategory[$key]['post_date'] = $val->post_date;
        $postByCategory[$key]['post_title'] = $val->post_title;
        $postByCategory[$key]['post_type'] = $val->post_type;
        $postByCategory[$key]['post_name'] = $val->post_name;
        $postByCategory[$key]['ping_status'] = $val->ping_status;
    }
    $response = array(
        'catId' => $categoryId,
        'catText' => $categoryText,
        'postByCategory' => $postByCategory,
    );
    $output = json_encode($response);
    print_r($output);
    die();

}


add_action('wp_ajax_save_thumb', 'createThumbnail');
add_action('wp_ajax_nopriv_save_thumb', 'createThumbnail');

function createThumbnail()
{
    $url = $_POST['url'];

    $arrayPath = parse_url($url);
    $parsePath = explode('/', $arrayPath['path']);
    array_pop($parsePath);
    $implodePath = implode('/', $parsePath);
    $dirName = pathinfo($url, PATHINFO_DIRNAME);
    $extension = pathinfo($url, PATHINFO_EXTENSION);
    $fileName = pathinfo($url, PATHINFO_FILENAME);

    $thumb = $implodePath . '/' . $fileName . '_thumb.' . $extension;
    $PathToThumb = $dirName . '/' . $fileName . '_thumb.' . $extension;


    if (!file_exists(ABSPATH . $thumb)) {
        $image = wp_get_image_editor($url);
        if (!is_wp_error($image)) {
            $image->resize(310, 230, true);
            $image->save(ABSPATH . $thumb);
        }
        echo $PathToThumb;
        die();
    } else {
        echo $PathToThumb;
        die();
    }
}

function getAddressLatLng($address){
    $address = urlencode(strip_tags($address));

    $url = "http://maps.googleapis.com/maps/api/geocode/json?address=$address&sensor=false";
    $raw = file_get_contents($url);
    $data = json_decode($raw);

    $info['lat'] = $data->results[0]->geometry->location->lat;
    $info['lng'] = $data->results[0]->geometry->location->lng;

    return $info;
}


function theme_page_options()
{
    global $themename;

    $file_dir = get_bloginfo('template_directory');
    wp_enqueue_script("{$themename}_js_jquery_ui", $file_dir . "/functions/js/jquery-ui.js", false, "1.0");
    wp_enqueue_script("{$themename}_js_jquery_scroll_follow", $file_dir . "/functions/js/jquery.scroll-follow.js", false, "1.0");
    wp_enqueue_media();
    wp_enqueue_script('thickbox');
    wp_enqueue_script('media-upload');

    wp_enqueue_style('thickbox');
    wp_enqueue_script("{$themename}_js_header", $file_dir . "/functions/js/header.js", false, "1.0");
    wp_enqueue_script("{$themename}_js_impressionismGallery", $file_dir . "/functions/js/gallery.js", false, "1.0");

    global $theme_options_menu;

    if ($_POST[$themename.'_menu_submit']) {
        $_POST = array_map('stripslashes_deep', $_POST);

        if(!empty($_POST[$themename.'_contact_us_block']['address'])){
            $latLng = getAddressLatLng($_POST[$themename.'_contact_us_block']['address']);
        }

        $new_values = array(
            $themename.'_menu_title' => $_POST[$themename.'_menu_title'],
            $themename.'_menu_header_width' => htmlspecialchars($_POST[$themename.'_menu_header_width'], ENT_QUOTES),
            $themename.'_header_site_name' => htmlspecialchars($_POST[$themename.'_header_site_name'], ENT_QUOTES),
            $themename.'_header_site_slogan' => htmlspecialchars($_POST[$themename.'_header_site_slogan'], ENT_QUOTES),
            $themename.'_slider_images' => $_POST[$themename.'_images'],
            $themename.'_portfolio_image' => $_POST[$themename.'_portfolio'],
            $themename.'_portfolio_block_title' => $_POST[$themename.'_portfolio_block_title'],
            $themename.'_about_block_title' => $_POST[$themename.'_about_block_title'],
            $themename.'_about_block_content' => $_POST[$themename.'_about'],
            $themename.'_our_team_block_title' => $_POST[$themename.'_our_team_block_title'],
            $themename.'_blog_block' => $_POST[$themename.'_blog_block'],
            $themename.'_contact_us_block' => $_POST[$themename.'_contact_us_block'],
            $themename.'_contact_us_block_address' => array('lat' => $latLng['lat'], 'lng' => $latLng['lng']),
            $themename.'_social_network' => $_POST[$themename.'_social_network'],
            $themename.'_social_network_custom' => $_POST[$themename.'_social_network_custom'],
        );
        update_option($themename.'_theme_options_menu', $new_values);
        $theme_options_menu = $new_values;
    }
    ?>
    <div class="wrap general-settings">
    <table style="width: 100%; position: relative;">
        <tr>
            <td><h2>Theme Art Nouveau Options</h2></td>
            <td style="width: 270px">
                <span id="impressionism-save-settings" class="round-button">
                    Save Settings
                    <span id="impressionism-save-settings-counter" style="display: none"></span>
                </span>
                <span class="info"></span>
                <ul class="docs">
                    <li><a href="<?php echo get_bloginfo('template_directory')?>/docs/help.html" target="_blank">Documentations</a></li>
                    <li><a href="<?php echo get_bloginfo('template_directory')?>/features/features.html" target="_blank">Features</a></li>
                </ul>
            </td>
        </tr>
    </table>


    <div class="admin-general-block">
    <ul class="admin-menu">
        <li class="js-header"><a href="#" class="selected">Header</a></li>
        <li class="js-slideshow"><a href="#">Slide Show</a></li>
        <li class="js-about"><a href="#">About</a></li>
        <li class="js-our-team"><a href="#">Our Team</a></li>
        <li class="js-portfolio"><a href="#">Portfolio</a></li>
        <li class="js-blog"><a href="#">Blog</a></li>
        <li class="js-contact-us"><a href="#">Contact Us</a></li>
        <li class="js-social"><a href="#">Social Network</a></li>
    </ul>
    <form action="themes.php?page=themeoptions" method="post" id="impressionism-form-settings">
    <div class="admin-menu-content">

    <div class="js-header">
        <h2>Main Menu</h2>

        <div id="sortable">
            <?php $i = 0;
            foreach ($theme_options_menu[$themename.'_menu_title'] as $k => $v): ?>
                <div class="group">
                    <label for="">Menu <?php echo $i + 1 ?></label>
                    <input type="text" name=<?php echo $themename;?>_menu_title[<?php echo $i ?>][title]"
                           value="<?php echo $v['title'] ?>"/>
                    <label for="">Link <?php echo $i + 1 ?></label>
                    <input type="text" name="<?php echo $themename;?>_menu_title[<?php echo $i ?>][link]"
                           value="<?php echo $v['link'] ?>"/>
                </div>
                <?php $i++; endforeach; ?>
        </div>
        <h2>Header pixel width</h2>

        <div class="group">
            <label for="amount">Header [px] Width:</label>
            <input type="text" name="<?php echo $themename;?>_menu_header_width"
                   value="<?php echo $theme_options_menu[$themename.'_menu_header_width'] ?>" id="amount"
                   style="border: 0; color: #f6931f; font-weight: bold; width: 30px; margin-left: 0px; margin-right: 0px; padding-left: 0px; padding-right: 0px;"/><span
                style="color: #f6931f; font-weight: bold;">pixels</span>
        </div>
        <div id="slider-range-min" style="width: 400px"></div>

        <h2>Site Name</h2>

        <div class="group">
            <label for="">Site Name:</label> <input type="text" name="<?php echo $themename;?>_header_site_name"
                                                    value="<?php echo $theme_options_menu[$themename.'_header_site_name'] ?>"/>
        </div>
        <div class="group">
            <label for="" class="textarea-label">Slogan:</label> <textarea
                name="<?php echo $themename;?>_header_site_slogan"><?php echo $theme_options_menu[$themename.'_header_site_slogan'] ?></textarea>
        </div>

        <div class="group" style="display: none">
            <label for=""></label><input type="text" name="<?php echo $themename;?>_menu_submit" value="Save"/>
        </div>

    </div>
    <div class="js-slideshow slideshow">
        <h2>Home Slider</h2>

        <div id="div_inputs">
            <?php if ($theme_options_menu[$themename.'_slider_images']): ?>
                <?php foreach ($theme_options_menu[$themename.'_slider_images'] as $key => $val): ?>
                    <div data-id="<?php echo $key; ?>" class="js-image-parent-block image-parent-block">
                        <span class="js-delete-button delete-button"></span>

                        <div class="media-block">
                            <div class="gallery-text text">Picture</div>
                            <div class="gallery-input input">
                                <a class="button choose-from-library-link" href="#"
                                   data-update-link="<?php echo $key; ?>">Open Media Library</a>

                                <div class="select-image-description">Choose your image, then click "Select" to
                                    apply it.
                                </div>
                                <?php if ($val['url']) {
                                    $style = 'inline-block';
                                } else {
                                    $style = 'none';
                                } ?>
                                <input class="imgUrl" style="display: <?php echo $style; ?>;"
                                       id="inp<?php echo $key; ?>" type="text"
                                       name="<?php echo $themename;?>_images[<?php echo $key; ?>][url]"
                                       value="<?php echo $val['url']; ?>"/>
                            </div>
                        </div>
                        <div class="link-block">
                            <div class="link-text text">Link</div>
                            <div class="link-input input"><input class="link" id="inp<?php echo $key; ?>"
                                                                 type="text"
                                                                 name="<?php echo $themename;?>_images[<?php echo $key; ?>][link]"
                                                                 value="<?php echo $val['link']; ?>"
                                                                 placeholder="http://"/></div>
                        </div>
                        <div class="caption-block">
                            <div class="caption-text text">Caption</div>
                            <div class="caption-input input"><input id="inp<?php echo $key; ?>" type="text"
                                                                    name="<?php echo $themename;?>_images[<?php echo $key; ?>][caption]"
                                                                    value="<?php echo $val['caption']; ?>"/>
                            </div>
                        </div>
                        <div class="description-block">
                            <div class="description-text text">Description</div>
                            <div class="description-input input"><textarea
                                    name="<?php echo $themename;?>_images[<?php echo $key; ?>][description]"><?php echo $val['description']; ?></textarea>
                            </div>
                        </div>
                        <div class="sortable-handler"></div>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
        <span id="add-new-slide" class="round-button">Add New Slide</span>
    </div>
    <div class="js-about">
        <h2>About Block</h2>
        <span class="portfolio-block-title block-title">Block Title</span> <input type="text" class="portfolio-block-input"
                                                                                  name="<?php echo $themename;?>_about_block_title"
                                                                                  value="<?php echo $theme_options_menu[$themename.'_about_block_title']; ?>"/>

        <div id="div_inputs_about">
            <?php if ($theme_options_menu[$themename.'_about_block_content']): ?>
                <?php foreach ($theme_options_menu[$themename.'_about_block_content'] as $key => $value): ?>
                    <div data-id="<?php echo $key; ?>" class="js-image-parent-block image-parent-block">
                        <span class="js-delete-button delete-button"></span>

                        <div class="media-block">
                            <div class="gallery-text text">Picture</div>
                            <div class="gallery-input input">
                                <a class="button choose-from-library-link" href="#" data-update-link="<?php echo $key; ?>">Open
                                    Media Library</a>

                                <div class="select-image-description">Choose your image, then click "Select" to apply it.
                                </div>
                                <?php if ($value['url']) {
                                    $style = 'inline-block';
                                } else {
                                    $style = 'none';
                                } ?>
                                <input class="imgUrl" style="display: <?php echo $style; ?>;" id="inp<?php echo $key; ?>"
                                       type="text" name="<?php echo $themename;?>_about[<?php echo $key; ?>][url]"
                                       value="<?php echo $value['url']; ?>"/>
                            </div>
                        </div>
                        <div class="link-block">
                            <div class="link-text text">Name</div>
                            <div class="link-input input"><input class="link" id="inp' + col_inputs + '" type="text"
                                                                 name="<?php echo $themename;?>_about[<?php echo $key; ?>][name]"
                                                                 value="<?php echo $value['name']; ?>"/></div>
                        </div>
                        <div class="caption-block">
                            <div class="caption-text text">Status</div>
                            <div class="caption-input input"><input id="inp<?php echo $key; ?>" type="text"
                                                                    name="<?php echo $themename;?>_about[<?php echo $key; ?>][status]"
                                                                    value="<?php echo $value['status']; ?>"/></div>
                        </div>
                        <div class="description-block">
                            <div class="description-text text">Description</div>
                            <div class="description-input input"><textarea
                                    name="<?php echo $themename;?>_about[<?php echo $key; ?>][description]"><?php echo $value['description']; ?></textarea>
                            </div>
                        </div>
                        <div class="sortable-handler"></div>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
        <span id="add-new-block-about" class="round-button">Add New Block</span>
    </div>
    <div class="js-our-team">
        <h2>Our Team Block</h2>
        <span class="portfolio-block-title block-title">Block Title</span> <input type="text" class="portfolio-block-input"
                                                                                  name="<?php echo $themename;?>_our_team_block_title[title]"
                                                                                  value="<?php echo $theme_options_menu[$themename.'_our_team_block_title']['title']; ?>"/>

        <div id="div_inputs_our_team">
            <div data-id="1" class="js-image-parent-block image-parent-block">
                <div class="media-block">
                    <div class="gallery-text text">Picture</div>
                    <div class="gallery-input input">
                        <a class="button choose-from-library-link" href="#" data-update-link="1">Open Media Library</a>

                        <div class="select-image-description">Choose your image, then click "Select" to apply it.</div>
                        <?php if ($theme_options_menu[$themename.'_our_team_block_title']['url']) {
                            $style = 'inline-block';
                        } else {
                            $style = 'none';
                        } ?>
                        <input class="imgUrl" style="display: <?php echo $style ?>;" id="inp1" type="text"
                               name="<?php echo $themename;?>_our_team_block_title[url]"
                               value="<?php echo $theme_options_menu[$themename.'_our_team_block_title']['url']; ?>"/>
                    </div>
                </div>
                <div class="description-block">
                    <div class="description-text text">Description</div>
                    <div class="description-input input"><textarea
                            name="<?php echo $themename;?>_our_team_block_title[description]"><?php echo $theme_options_menu[$themename.'_our_team_block_title']['description']; ?></textarea>
                    </div>
                </div>
                <div class="image-float-block">
                    <div class="image-float-text text">Image Float</div>
                    <div class="image-float-input input">
                        Left:&nbsp;&nbsp;<input type="radio" name="<?php echo $themename;?>_our_team_block_title[float]"
                                                value="left" <?php echo $theme_options_menu[$themename.'_our_team_block_title']['float'] == 'left' ? 'checked' : ''; ?> />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        Right:&nbsp;&nbsp;<input type="radio" name="<?php echo $themename;?>_our_team_block_title[float]"
                                                 value="right" <?php echo $theme_options_menu[$themename.'_our_team_block_title']['float'] == 'right' ? 'checked' : ''; ?> />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="js-portfolio">
        <h2>Portfolio Block</h2>
        <span class="portfolio-block-title block-title">Block Title</span> <input type="text" class="portfolio-block-input"
                                                                                  name="<?php echo $themename;?>_portfolio_block_title"
                                                                                  value="<?php echo $theme_options_menu[$themename.'_portfolio_block_title']; ?>"/>

        <div id="div_inputs_portfolio">
            <?php if ($theme_options_menu[$themename.'_portfolio_image']): ?>
                <?php foreach ($theme_options_menu[$themename.'_portfolio_image'] as $key => $value): ?>
                    <div data-id="<?php echo $key; ?>" class="image-parent-block">
                        <span class="js-delete-button delete-button"></span>

                        <div class="media-block">
                            <div class="media-text text">Picture</div>
                            <div class="media-input input">
                                <a class="button choose-from-library-link" href="#" data-update-link="<?php echo $key; ?>">Open
                                    Media Library</a>
                                <span class="js-create-thumb-process-image create-thumb-process-image"></span>

                                <div class="select-image-description">Choose your image, then click "Select" to apply it.
                                </div>
                                <?php if ($value['url']) {
                                    $style = 'inline-block';
                                } else {
                                    $style = 'none';
                                } ?>
                                <input class="imgUrl" style="display: <?php echo $style; ?>;" id="inp<?php echo $key; ?>"
                                       type="text" name="<?php echo $themename;?>_portfolio[<?php echo $key; ?>][url]"
                                       value="<?php echo $value['url']; ?>"/>
                                <input class="imgUrlThumb" style="display: none;" id="inp<?php echo $key; ?>" type="text"
                                       name="<?php echo $themename;?>_portfolio[<?php echo $key; ?>][thumb]"
                                       value="<?php echo $value['thumb']; ?>"/>
                            </div>
                        </div>
                        <div class="title-block">
                            <div class="title-text text">Title</div>
                            <div class="title-input input"><input class="link" id="inp<?php echo $key; ?>" type="text"
                                                                  name="<?php echo $themename;?>_portfolio[<?php echo $key; ?>][title]"
                                                                  value="<?php echo $value['title']; ?>"/></div>
                        </div>
                        <div class="sortable-handler"></div>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
        <span id="add-new-image-portfolio" class="round-button">Add New Image</span>
    </div>
    <div class="js-blog">
        <h2>Blog Block</h2>

        <div>
            <span class="portfolio-block-title block-title">Block Title</span> <input type="text"
                                                                                      class="portfolio-block-input"
                                                                                      name="<?php echo $themename;?>_blog_block[title]"
                                                                                      value="<?php echo $theme_options_menu[$themename.'_blog_block']['title']; ?>"/>
        </div>
        <div>
            <span class="portfolio-block-title block-title">Show default image</span>
            <input type="checkbox"  class="portfolio-block-input" name="<?php echo $themename;?>_blog_block[default_image]" <?php echo $theme_options_menu[$themename.'_blog_block']['default_image'] ? 'checked' : ''; ?> />
        </div>
        <div>
            <span class="portfolio-block-title block-title">Align Photo</span>
            Left:&nbsp;&nbsp;<input type="radio" class="" name="<?php echo $themename;?>_blog_block[photo_float]"
                                    value="left" <?php echo $theme_options_menu[$themename.'_blog_block']['photo_float'] == 'left' ? 'checked' : ''; ?> />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            Right:&nbsp;&nbsp;<input type="radio" class="" name="<?php echo $themename;?>_blog_block[photo_float]"
                                     value="right" <?php echo $theme_options_menu[$themename.'_blog_block']['photo_float'] == 'right' ? 'checked' : ''; ?> />
        </div>
    </div>
    <div class="js-contact-us">
        <h2>Contact Us Block</h2>

        <div>
            <span class="portfolio-block-title block-title">Block Title</span>
            <input type="text" class="portfolio-block-input" name="<?php echo $themename;?>_contact_us_block[title]"
                   value="<?php echo $theme_options_menu[$themename.'_contact_us_block']['title']; ?>"/>
        </div>
        <div>
            <span class="portfolio-block-title block-title float-textarea">Address Line</span>
            <input type="text" class="portfolio-block-input" name="<?php echo $themename;?>_contact_us_block[address]"
                   value="<?php echo $theme_options_menu[$themename.'_contact_us_block']['address']; ?>"/>
        </div>
        <div>
            <span class="portfolio-block-title block-title">Show map</span>
            <input type="checkbox" <?php echo $theme_options_menu[$themename.'_contact_us_block']['show_map'] ? 'checked' : ''; ?>
                   class="portfolio-block-input" name="<?php echo $themename;?>_contact_us_block[show_map]"/>
        </div>
        <br/>

        <div>
            <span class="portfolio-block-title block-title">Phone Line</span>
            <input type="text" class="portfolio-block-input" name="<?php echo $themename;?>_contact_us_block[phone]"
                   value="<?php echo $theme_options_menu[$themename.'_contact_us_block']['phone']; ?>"/>
        </div>
        <div>
            <span class="portfolio-block-title block-title">Email Line</span>
            <input type="text" class="portfolio-block-input" name="<?php echo $themename;?>_contact_us_block[email]"
                   value="<?php echo $theme_options_menu[$themename.'_contact_us_block']['email']; ?>"/>
        </div>
        <div>
            <span class="portfolio-block-title block-title">Marker Title</span>
            <input type="text" class="portfolio-block-input" name="<?php echo $themename;?>_contact_us_block[map_marker_title]"
                   value="<?php echo $theme_options_menu[$themename.'_contact_us_block']['map_marker_title']; ?>"/>
        </div>
        <div>
            <span class="portfolio-block-title block-title float-textarea">Map Info Window Text</span>
            <textarea type="text" class="portfolio-block-input"
                      name="<?php echo $themename;?>_contact_us_block[map_window_text]"><?php echo $theme_options_menu[$themename.'_contact_us_block']['map_window_text']; ?></textarea>
        </div>
    </div>
    <div class="js-social">
        <h2>Social Network</h2>

        <div>
            <span class="portfolio-block-title block-title">Facebook</span>
            <input type="text" class="portfolio-block-input" name="<?php echo $themename;?>_social_network_custom[facebook]"
                   value="<?php echo $theme_options_menu[$themename.'_social_network_custom']['facebook']; ?>"
                   placeholder="http://"/>
        </div>
        <div>
            <span class="portfolio-block-title block-title">Flickr</span>
            <input type="text" class="portfolio-block-input" name="<?php echo $themename;?>_social_network_custom[flickr]"
                   value="<?php echo $theme_options_menu[$themename.'_social_network_custom']['flickr']; ?>" placeholder="http://"/>
        </div>
        <div>
            <span class="portfolio-block-title block-title">LinkedIn</span>
            <input type="text" class="portfolio-block-input" name="<?php echo $themename;?>_social_network_custom[linkedin]"
                   value="<?php echo $theme_options_menu[$themename.'_social_network_custom']['linkedin']; ?>"
                   placeholder="http://"/>
        </div>
        <div>
            <span class="portfolio-block-title block-title">MySpace</span>
            <input type="text" class="portfolio-block-input" name="<?php echo $themename;?>_social_network_custom[myspace]"
                   value="<?php echo $theme_options_menu[$themename.'_social_network_custom']['myspace']; ?>"
                   placeholder="http://"/>
        </div>
        <div>
            <span class="portfolio-block-title block-title">Twitter</span>
            <input type="text" class="portfolio-block-input" name="<?php echo $themename;?>_social_network_custom[twitter]"
                   value="<?php echo $theme_options_menu[$themename.'_social_network_custom']['twitter']; ?>"
                   placeholder="http://"/>
        </div>
        <div>
            <span class="portfolio-block-title block-title">Vimeo</span>
            <input type="text" class="portfolio-block-input" name="<?php echo $themename;?>_social_network_custom[vimeo]"
                   value="<?php echo $theme_options_menu[$themename.'_social_network_custom']['vimeo']; ?>" placeholder="http://"/>
        </div>
        <div>
            <span class="portfolio-block-title block-title">Tumblr</span>
            <input type="text" class="portfolio-block-input" name="<?php echo $themename;?>_social_network_custom[tumblr]"
                   value="<?php echo $theme_options_menu[$themename.'_social_network_custom']['tumblr']; ?>" placeholder="http://"/>
        </div>
        <div id="div_inputs_social">
            <?php if ($theme_options_menu[$themename.'_social_network']): ?>
                <?php foreach ($theme_options_menu[$themename.'_social_network'] as $key => $value): ?>
                    <div data-id="<?php echo $key; ?>" class="image-parent-block">
                        <span class="js-delete-button delete-button"></span>

                        <div class="media-block">
                            <div class="media-text text">Picture</div>
                            <div class="media-input input">
                                <a class="button choose-from-library-link" href="#" data-update-link="<?php echo $key; ?>">Open
                                    Media Library</a>
                                <span class="js-create-thumb-process-image create-thumb-process-image"></span>

                                <div class="select-image-description">Choose your image, then click "Select" to apply it.
                                </div>
                                <?php if ($value['url']) {
                                    $style = 'inline-block';
                                } else {
                                    $style = 'none';
                                } ?>
                                <input class="imgUrl" style="display: <?php echo $style; ?>;" id="inp<?php echo $key; ?>"
                                       type="text" name="<?php echo $themename;?>_social_network[<?php echo $key; ?>][url]"
                                       value="<?php echo $value['url']; ?>"/>
                            </div>
                        </div>
                        <div class="title-block">
                            <div class="title-text text">Link</div>
                            <div class="title-input input"><input class="link" id="inp<?php echo $key; ?>" type="text"
                                                                  name="<?php echo $themename;?>_social_network[<?php echo $key; ?>][link]"
                                                                  value="<?php echo $value['link']; ?>"
                                                                  placeholder="http://"/></div>
                        </div>
                        <div class="sortable-handler"></div>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
        <span id="add-new-social-network" class="round-button">Add Social Network</span>
    </div>
    </form>
    </div>
    </div>

<?php
}
add_action('admin_init', $themename.'_add_init');
add_action('admin_menu', $themename.'_page_add');


register_sidebar(array('name' => 'sidebar1'));
register_sidebar(array('name' => 'sidebar2'));



function include_widget() {
    require( TEMPLATEPATH . '/art_nouveau-widgets.php' );
}
add_action( 'after_setup_theme', 'include_widget' );
?>

