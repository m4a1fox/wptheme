<?php get_header();?>
<?php
    $options = get_option('art_nouveautheme_options_menu');
    $currentCategoryId = get_query_var('cat');
    $currentCategoryText = get_the_category_by_ID($currentCategoryId);
?>
<div class="fixed-width category-block" data-current-id="<?php echo $currentCategoryId?>" data-current-text="<?php echo $currentCategoryText?>">
    <h2 class="font-swaak">Category</h2>
    <div class="blogBlock ">
        <div class="blogCategories">
            <select class="js-categories-dd">
                <?php foreach (get_categories() as $key => $value): ?>
                    <?php if($value->term_id != 1):?>
                        <option value="<?php echo $value->term_id; ?>"
                                data-text="<?php echo $value->cat_name ?>"><?php echo $value->cat_name; ?></option>
                    <?php endif;?>
                <?php endforeach; ?>

            </select>
        </div>
    </div>

    <div class="post blog-post category-list-blog ">

        <?php if (have_posts()) { ?>
            <?php while (have_posts()) {
                the_post(); ?>
                <?php $content = catch_that_image(get_the_content('more')); ?>
                    <div class="entry">
                        <img src="<?php echo $content['image'] ?>" alt="" align="left" style="margin-bottom: 5px;"/>

                        <div class="post-title">
                            <h1><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h1>

                            <div class="post-date"><span class="post-month"><?php the_time('d.m.Y') ?></span></div>
                        </div>
                        <?php echo $content['text']; ?>
                        <div
                            class="blogControl <?php echo $options['art_nouveaublog_block']['photo_float'] == 'right' ? 'right' : ''; ?>">
                            <div class="fluid width-50 text-align-left">
                                <p><?php echo getCategoryPostList(get_the_ID()); ?></p>
                            </div>
                            <div class="fluid width-50 text-align-right"><p>
                                    Comments: <?php echo wp_count_comments(get_the_ID())->total_comments; ?></p></div>
                        </div>
                        <div style="height: 20px"></div>
                    </div>
            <?php } ?>
    </div>

    <div class="block-widgets js-block-widgets position-relative">

            <?php get_sidebar(); ?>


    </div>
</div>


    <?php } ?>

<div class="secondary-page contact rounded"></div>
<div class="content-block contactBlock contact-secondary-page">
    <?php get_footer();?>
