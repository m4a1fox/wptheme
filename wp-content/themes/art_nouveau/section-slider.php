<?php $options = get_option('art_nouveau_theme_options_menu');?>
<div class="content-block greyBlock border-tb">
    <div class="fixed-width">
        <div class="greyBlockHead z-index-1 position-absolute"></div>
        <div class="greyBlockContent z-index-2 position-relative">
            <ul class="js-carousel clear-ul vertical-list">
                <?php if($options['art_nouveau_slider_images']):?>
                    <?php foreach($options['art_nouveau_slider_images'] as $key=>$value):?>
                        <li>
                            <img src="<?php echo $value['url']?>" alt="slider image"/>
                            <div class="padded-t">
                                <div class="fluid width-65">
                                    <p class=""><?php echo $value['description']?></p>
                                </div>
                                <div class="fluid width-35">
                                    <div class="text-align-right">
                                        <a href="<?php echo $value['link']?>" class="anchor-big"><?php echo $value['caption']?></a>
                                    </div>
                                </div>
                            </div>
                        </li>
                    <?php endforeach;?>
                <?php else:?>
                    <li>
                        <img src="<?php echo get_template_directory_uri();?>/img/slider-image.png" alt="slider image"/>
                        <div class="padded-t">
                            <div class="fluid width-65">
                                <p class="">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                    incididunt ulabore et dolore magna aliqua. Ut enim ad minim veniam,
                                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
                                    commodo consequat.
                                </p>
                            </div>
                            <div class="fluid width-35">
                                <div class="text-align-right">
                                    <a href="#" class="anchor-big">See more features</a>
                                </div>
                            </div>
                        </div>
                    </li>
                <?php endif;?>
            </ul>
        </div>
    </div>
</div>