<?php $options = get_option('art_nouveau_theme_options_menu');?>
<?php get_header(); ?>
<div class="header">
    <div class="header-bg">
        <div class="fixed-width" style="width: <?php echo $options['impressionism_menu_header_width'];?>px;">
            <div class="nav font-swaak text-align-center">
                <ul>
                    <?php foreach($options['art_nouveau_menu_title'] as $val):?>
                        <li><a href="<?php echo $val['link']?>" class=""><?php echo $val['title']?></a></li>
                    <?php endforeach;?>
                </ul>
            </div>
            <a href="/" class="no-decoration">
                <div class="header-logo text-align-center">
                    <h1 class="slogan font-swaak"><?php echo $options['art_nouveau_header_site_name'];?></h1>
                    <h3 class="sub-slogan"><?php echo $options['art_nouveau_header_site_slogan'];?></h3>
                </div>
            </a>
        </div>
    </div>
</div>