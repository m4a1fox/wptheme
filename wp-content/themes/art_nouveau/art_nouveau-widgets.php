<?php
/*
 *	Plugin Name: Art Widgets
 *	Version: 1.0
 *	Author: e.abdurayimov
 */

add_action('widgets_init', 'register_art_nouveau_widgets');

function register_art_nouveau_widgets() {
	register_widget('Flickr');
	register_widget('Video');
	register_widget('Twitter');
	register_widget('Latest');
	register_widget('Google');
}

class Settings {

	function get_video__type($url, $type = 'page') {
		if ( preg_match( '/youtube\.com\/watch\?v=[^&]+/', $url ) ) {
			$type = 'youtube';
		}
		elseif ( preg_match( '/vimeo\.com\/[0-9]+/i', $url ) ) {
			$type = 'vimeo';
		}
		return $type;
	}

	function get_video_id( $url, $id = '' ) {
		if ( $this->get_video__type( $url ) == 'youtube' ) {
			parse_str( parse_url( $url, PHP_URL_QUERY ), $youtube );
			$id = $youtube['v'];
		}
		else if ( $this->get_video__type( $url ) == 'vimeo' ) {
			$result = preg_match('/(\d+)/', $url, $matches);
			$id = $matches[0];
		}
		return $id;
	}

	function get_tweets( $user, $number = 3, $tweets = array() ) {
		$user = str_replace( '@', '', $user );
		$tweets_feed = $this->get_contents( 'http://api.twitter.com/1/statuses/user_timeline.json?include_rts=1&screen_name=' . $user . '&count=' . $number . '&callback=?' );
		$tweets_feed = json_decode( $tweets_feed, true );
		if ( ! empty( $tweets_feed ) && ! $tweets_feed["error"] ) {
			foreach ( $tweets_feed as $tweet ) {
				$tweets_array[] = array(
					'content' => html_entity_decode( $tweet['text'] ),
					'link' => 'http://twitter.com/' . $user . '/status/' . $tweet['id'],
					'datetime' => date( DATE_W3C, strtotime( $tweet['created_at'] ) ) ,
					'date' => $this->time_ago( $tweet['created_at'] )
				);
			}
		}
		return $tweets_array;
	}

	function time_ago($date) {
		if(empty($date)) {
			return "ERROR: No date provided";
		}
		$periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
		$lengths = array("60","60","24","7","4.35","12","10");
		$now = time();
		$unix_date = strtotime($date);
		// check validity of date
		if(empty($unix_date)) {
			return "ERROR: Invalid date";
		}
		// is it future date or past date
		if($now > $unix_date) {
			$difference = $now - $unix_date;
			$tense = "ago";
		}
		else {
			$difference = $unix_date - $now;
			$tense = "from now";
		}
		for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
			$difference /= $lengths[$j];
		}
		$difference = round($difference);
		if($difference != 1) {
//      $periods[$j] .= "s"; // plural for English language
			$periods = array("seconds", "minutes", "hours", "days", "weeks", "months", "years", "decades"); // plural for international words
		}
		return "$difference $periods[$j] {$tense}";
	}
	function get_contents( $url, $content = null ) {
		if ( function_exists('curl_init') ) {
			$ch = curl_init( $url );
			curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch, CURLOPT_HEADER, 0 );
			curl_setopt( $ch, CURLOPT_TIMEOUT, 10 );
			$content = curl_exec( $ch );
			curl_close( $ch );
		}
		else {
			$content = file_get_contents( $url );
		}
		return $content;
	}

}

/*
 *	Flickr
 */
class Flickr extends WP_Widget {

	function Flickr() {
		$widget_ops = array( 'classname' => 'widget-art-flickr', 'description' => __('Widget makes it easy to show thumbnails of user latest Flickr photos in your sidebar' ) );
		$control_ops = array( 'width' => '', 'height' => '' );
		$this->WP_Widget( 'widget-art-flickr', __('- Art Flickr Widget'), $widget_ops, $control_ops );
	}

	function widget( $args, $instance ) {
		extract($args);

		$title = $instance['title'];
		$id = $instance['id'];
		$number = $instance['number'];
		$display = $instance['order'];

		echo $before_widget;

		if ( $title ) {
			echo $before_title . $title . $after_title;
		}
		?>

		<style>
			.flickr_badge_image {
				float: left;
				margin: 0 10px 8px 0;
				padding: 0;
			}
			.flickr_badge_image img {
				width: 50px;
				height: 50px;
			}
		</style>

		<div class="art-block-widget-flickr">
			<script type="text/javascript" src="http://www.flickr.com/badge_code_v2.gne?count=<?php echo $number; ?>&amp;display=<?php echo $display; ?>&amp;size=s&amp;layout=x&amp;source=user&amp;user=<?php echo $id; ?>"></script>
		</div>

		<?php
		echo $after_widget;
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['id'] = strip_tags( $new_instance['id'] );
		$instance['number'] = ( intval( $new_instance['number'] ) && $new_instance['number'] < 11 ? $new_instance['number'] : $instance['number'] );
		$instance['order'] = $new_instance['order'];

		return $instance;
	}

	function form($instance) {
		$defaults = array( 'title' => __('Flickr'), 'number' => 10, 'order' => 'random' );
		$instance = wp_parse_args( (array) $instance, $defaults );
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Widget title:'); ?></label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" class="widefat" style="width: 215px;" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'id' ); ?>"><?php _e('User ID:'); ?></label>
			<input id="<?php echo $this->get_field_id( 'id' ); ?>" name="<?php echo $this->get_field_name( 'id' ); ?>" value="<?php if ( isset( $instance['id'] ) ) echo $instance['id']; ?>" class="widefat" style="width: 215px;" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e('Number of photos:'); ?></label>
			<input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" value="<?php echo $instance['number']; ?>" class="widefat" style="width: 215px;" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'order' ); ?>"><?php _e('Ordering photos:'); ?></label>
			<select id="<?php echo $this->get_field_id('order'); ?>" name="<?php echo $this->get_field_name('order'); ?>" class="widefat" style="width: 215px">
				<option <?php selected( $instance['order'], 'random' ); ?> value="random"><?php _e('Random'); ?></option>
				<option <?php selected( $instance['order'], 'latest' ); ?> value="latest"><?php _e('Latest'); ?></option>
			</select>
		</p>

	<?php

	}
}

/*
 * Video
 */
class Video extends WP_Widget {

	function Video() {
		$widget_ops = array( 'classname' => 'widget-art-video', 'description' => __('A widget that allows you to easily insert YouTube or Vimeo videos into sidebar.' ) );
		$control_ops = array( 'width' => '', 'height' => '' );
		$this->WP_Widget( 'widget-art-video', __('- Art Video Widget'), $widget_ops, $control_ops );
	}

	function widget( $args, $instance ) {
		extract($args);

		$title = $instance['title'];
		$url = $instance['url'];
		$size = esc_attr( $instance['size'] );
		$size = explode( 'x', $size );

		echo $before_widget;

		$settings = new Settings();

		if ($title) {
			echo $before_title . $title . $after_title;
		}

		if ( $url ) : ?>
			<div class="art-block-widget-video">
				<?php if ( $settings->get_video__type( $url ) == 'youtube' ) : $id = $settings->get_video_id( $url ); ?>
					<iframe type="text/html" width="<?php echo $size[ 0 ]; ?>" height="<?php echo $size[ 1 ]; ?>" src="http://www.youtube.com/embed/<?php echo $id; ?>?wmode=transparent" frameborder="0" allowfullscreen></iframe>
				<?php elseif ( $settings->get_video__type( $url ) == 'vimeo' ) : $id = $settings->get_video_id( $url ); ?>
					<iframe src="http://player.vimeo.com/video/<?php echo $id; ?>?title=0&amp;byline=0&amp;portrait=0" width="<?php echo $size[ 0 ]; ?>" height="<?php echo $size[ 1 ]; ?>" frameborder="0"></iframe>
				<?php endif; ?>
			</div>
		<?php
		endif;

		echo $after_widget;
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['url'] = esc_url( $new_instance['url'] );
		$instance['size'] = ( intval( $new_instance['size'] ) ? $new_instance['size'] : $instance['size'] );

		return $instance;
	}

	function form($instance) {
		$defaults = array('title' => __('Moving Pictures!'), 'size' => '255x180');

		$instance = wp_parse_args((array) $instance, $defaults); ?>

		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label><br />
			<input id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $instance['title']; ?>" class="widefat" style="width: 217px" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('url'); ?>"><?php _e('Video URL:'); ?></label><br />
			<input id="<?php echo $this->get_field_id('url'); ?>" placeholder="http://" name="<?php echo $this->get_field_name('url'); ?>" value="<?php if ( isset( $instance['url'] ) ) echo esc_url( $instance['url'] ); ?>" class="widefat" style="width: 217px" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('size'); ?>"><?php _e('Video size:'); ?></label><br />
			<input id="<?php echo $this->get_field_id('size'); ?>" name="<?php echo $this->get_field_name('size'); ?>" value="<?php if ( isset( $instance['size'] ) ) echo $instance['size']; ?>" class="widefat" style="width: 217px" />
		</p>

	<?php

	}
}

/*
 * Twitter
 */
class Twitter extends WP_Widget {

	function Twitter() {
		$widget_ops = array( 'classname' => 'widget-art-twitter', 'description' => __('Connect Twitter account and the widget will display latest tweets.' ) );
		$control_ops = array( 'width' => '', 'height' => '' );
		$this->WP_Widget( 'widget-art-twitter', __('- Art Twitter Widget'), $widget_ops, $control_ops );
	}

	function update($new_instance, $old_instance) {
		$instance = $old_instance;

		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['username'] = strip_tags( $new_instance['username'] );
		$instance['number'] = ( intval( $new_instance['number'] ) ? $new_instance['number'] : $instance['number'] );

		return $instance;
	}

	function form($instance) {

		$defaults = array('title' => __('Latest Tweets'), 'number' => 3);

		$instance = wp_parse_args((array) $instance, $defaults); ?>

		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label><br />
			<input id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $instance['title']; ?>" class="widefat" style="width: 217px" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('username'); ?>"><?php _e('Username:'); ?></label><br />
			<input id="<?php echo $this->get_field_id('username'); ?>" name="<?php echo $this->get_field_name('username'); ?>" value="<?php if ( isset( $instance['username'] ) ) echo $instance['username']; ?>" class="widefat" style="width: 217px" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('number'); ?>"><?php _e('Number:'); ?></label><br />
			<input id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" value="<?php echo $instance['number']; ?>" class="widefat" style="width: 217px" />
		</p>

	<?php
	}

	function widget($args, $instance) {

		extract($args);

		$title = $instance['title'];
		$username = $instance['username'];
		$number = $instance['number'];

		$settings = new Settings();

		$tweets = $settings->get_tweets( $username, $number );

		echo $before_widget;

		if ( $title ) {
			echo $before_title . $title . $after_title;
		}
		?>

		<ul>
			<?php if ( $tweets ) : foreach ( $tweets as $tweets => $tweet ) : ?>
				<li><span><?php echo $tweet['content']; ?></span> <a href="<?php echo $tweet['link']; ?>"><?php echo $tweet['date']; ?></a></li>
			<?php endforeach; else: ?>
				<p><em><?php printf( __('No tweets found from "%s"') , $instance['username'] ); ?></em></p>
			<?php endif; ?>
		</ul>

		<?php
		echo $after_widget;

	}
}

/*
 * Latest articles
 */
class Latest extends WP_Widget {

	function Latest() {
		$widget_ops = array( 'classname' => 'widget-art-latest', 'description' => __('' ) );
		$control_ops = array( 'width' => '', 'height' => '' );
		$this->WP_Widget( 'widget-art-latest', __('- Art Latest Articles Widget'), $widget_ops, $control_ops );
	}

	function update($new_instance, $old_instance) {
		$instance = $old_instance;

		$instance['title'] = strip_tags( $new_instance['title'] );

		$total_posts = wp_count_posts();

		$instance['number'] = ( intval( $new_instance['number'] ) ? ( $new_instance['number'] > $total_posts->publish ? $total_posts->publish : $new_instance['number'] ) : $instance['number'] );

		$instance['orderby'] = $new_instance['orderby'];
		$instance['order'] = $new_instance['order'];

		return $instance;
	}

	function form($instance) {

		$defaults = array('title' => __('Recent Articles'), 'picture' => 'on', 'number' => 3, 'orderby' => 'date', 'order' => 'DESC');

		$instance = wp_parse_args((array) $instance, $defaults); ?>

		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label><br />
			<input id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $instance['title']; ?>" class="widefat" style="width: 217px" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('number'); ?>"><?php _e('Number:'); ?></label><br />
			<input id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" value="<?php echo $instance['number']; ?>" class="widefat" style="width: 217px" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('orderby'); ?>"><?php _e('Order by:'); ?></label><br />
			<select id="<?php echo $this->get_field_id('orderby'); ?>" name="<?php echo $this->get_field_name('orderby'); ?>" class="widefat" style="width: 217px">
				<option <?php selected( $instance['orderby'], 'ID' ); ?> value="id">ID</option>
				<option <?php selected( $instance['orderby'], 'title' ); ?> value="title"><?php _e('Title'); ?></option>
				<option <?php selected( $instance['orderby'], 'date' ); ?> value="date"><?php _e('Date'); ?></option>
				<option <?php selected( $instance['orderby'], 'rand' ); ?> value="rand"><?php _e('Random'); ?></option>
				<option <?php selected( $instance['orderby'], 'comment_count' ); ?> value="comment_count"><?php _e('Popular'); ?></option>
			</select>
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('order'); ?>"><?php _e('Order:'); ?></label><br />
			<select id="<?php echo $this->get_field_id('order'); ?>" name="<?php echo $this->get_field_name('order'); ?>" class="widefat" style="width: 217px">
				<option <?php selected( $instance['order'], 'ASC' ); ?> value="ASC"><?php _e('Ascending'); ?></option>
				<option <?php selected( $instance['order'], 'DESC' ); ?> value="DESC"><?php _e('Descending'); ?></option>
			</select>
		</p>

	<?php
	}

	function widget($args, $instance) {

		extract($args);

		$settings = new Settings();

		$title = $instance['title'];
		$number = $instance['number'];
		$orderby = $instance['orderby'];
		$order = $instance['order'];

		$args = array(
			'numberposts' => $number
			,'orderby' => $orderby
			,'order' => $order
			,'post_type' => 'post'
			,'post_status' => 'publish'
		);

		$result = wp_get_recent_posts($args);

		echo $before_widget;

		if ( $title ) {
			echo $before_title . $title . $after_title;
		}
		?>

		<ul>

            <?php foreach($result as $post){  ?>
                <li>
					<h4><a title="<?php printf( __('Permanent Link to %s') , $post['post_title']); ?>" href="<?php echo get_permalink($post['ID']); ?>"><?php echo $post['post_title']; ?></a></h4>
				<span>
					<time datetime="<?php the_time( DATE_W3C ); ?>"><?php echo $settings->time_ago( $post['post_date'] ); ?></time>,
                    <a href="<?php echo $post['guid']?>#respond">
                        <?php echo $post['comment_count'] > 0 ? 'Comments: '.$post['comment_count'] : 'No Comments' ;?>
                    </a>

				</span>
				</li>

			<?php } ?>

		</ul>

		<?php
		echo $after_widget;

	}
}

/*
 * Google
 */
class Google extends WP_Widget {

	function Google() {
		$widget_ops = array( 'classname' => 'widget-art-google', 'description' => __('' ) );
		$control_ops = array( 'width' => '', 'height' => '' );
		$this->WP_Widget( 'widget-art-google', __('- Art Google Maps Widget'), $widget_ops, $control_ops );
	}


	function update($new_instance, $old_instance) {
		$instance = $old_instance;

		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['center'] = strip_tags( $new_instance['center'] );
		$instance['size'] = strip_tags( $new_instance['size'] );

		$instance['zoom'] = ( intval( $new_instance['zoom'] ) ? $new_instance['zoom'] : $instance['zoom'] );
		$instance['height'] = ( intval( $new_instance['height'] ) ? $new_instance['height'] : $instance['height'] );

		$instance['type'] = $new_instance['type'];

		return $instance;
	}

	function form($instance) {

		$defaults = array('title' => __('Google Maps'), 'center' => 'New York, NY', 'zoom' => 12, 'size' => '250x300', 'type' => 'roadmap');

		$instance = wp_parse_args((array) $instance, $defaults); ?>

		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label><br />
			<input id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $instance['title']; ?>" class="widefat" style="width: 217px" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('center'); ?>"><?php _e('Location:'); ?></label><br />
			<input id="<?php echo $this->get_field_id('center'); ?>" name="<?php echo $this->get_field_name('center'); ?>" value="<?php if ( isset( $instance['center'] ) ) echo $instance['center']; ?>" class="widefat" style="width: 217px" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('zoom'); ?>"><?php _e('Zoom:'); ?></label><br />
			<input id="<?php echo $this->get_field_id('zoom'); ?>" name="<?php echo $this->get_field_name('zoom'); ?>" value="<?php if ( isset( $instance['zoom'] ) ) echo $instance['zoom']; ?>" class="widefat" style="width: 217px" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('size'); ?>"><?php _e('Size:'); ?></label><br />
			<input id="<?php echo $this->get_field_id('size'); ?>" name="<?php echo $this->get_field_name('size'); ?>" value="<?php if ( isset( $instance['size'] ) ) echo $instance['size']; ?>" class="widefat" style="width: 217px" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('type'); ?>"><?php _e('Map type:'); ?></label><br />
			<select id="<?php echo $this->get_field_id('type'); ?>" name="<?php echo $this->get_field_name('type'); ?>" class="widefat" style="width: 217px">
				<option <?php selected( $instance['type'], 'roadmap' ); ?> value="roadmap"><?php _e('Roadmap'); ?></option>
				<option <?php selected( $instance['type'], 'satellite' ); ?> value="satellite"><?php _e('Satellite'); ?></option>
				<option <?php selected( $instance['type'], 'terrain' ); ?> value="terrain"><?php _e('Terrain'); ?></option>
				<option <?php selected( $instance['type'], 'hybrid' ); ?> value="hybrid"><?php _e('Hybrid'); ?></option>
			</select>
		</p>

	<?php
	}

	function widget($args, $instance) {

		extract($args);

		$title = $instance['title'];
		$center = urlencode( $instance['center'] );
		$zoom = urlencode( $instance['zoom'] );
		$size = urlencode( $instance['size'] );
		$type = urlencode( $instance['type'] );

		$map = '<img src="http://maps.googleapis.com/maps/api/staticmap?center=' . $center . '&zoom=' . $zoom . '&size='. $size .'&maptype='. $type . '&sensor=false" alt="" />';

		echo $before_widget;

		if ($title) {
			echo $before_title . $title . $after_title;
		}
		?>

		<?php echo $map; ?>

		<?php
		echo $after_widget;

	}

}

