<?php $options = get_option('art_nouveau_theme_options_menu');?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo('charset');?>" />
    <title><?php echo wp_title('<<', true, 'right');?> | <?php echo bloginfo('name');?></title>
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
    <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() . '/horizontal-slider.css';?>" type="text/css" media="screen" />
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() . '/custom-dropdown.css';?>" type="text/css" media="screen" />
    <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script type="text/javascript" src='<?php echo get_template_directory_uri() . '/js/jquery.custom-dropdown.js';?>'></script>
    <script type="text/javascript" src='<?php echo get_template_directory_uri() . '/js/jquery.horizontal-slider.js';?>'></script>
    <script type="text/javascript" src='<?php echo get_template_directory_uri() . '/js/script.js';?>'></script>
    <?php if(!empty($options['art_nouveau_contact_us_block_address']['lat']) && !empty($options['art_nouveau_contact_us_block_address']['lng']) && $options['art_nouveau_contact_us_block']['show_map']):?>
    <script type="text/javascript">
        (function() {
            window.onload = function(){
                // Creating a LatLng object containing the coordinate for the center of the map
                var latlng = new google.maps.LatLng(<?php echo $options['art_nouveau_contact_us_block_address']['lat'];?>, <?php echo $options['art_nouveau_contact_us_block_address']['lng'];?>);
                // Creating an object literal containing the properties we want to pass to the map
                var options = {
                    zoom: 13,
                    center: new google.maps.LatLng(<?php echo $options['art_nouveau_contact_us_block_address']['lat'];?>, <?php echo $options['art_nouveau_contact_us_block_address']['lng'];?>),
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                // Calling the constructor, thereby initializing the map
                var map = new google.maps.Map(document.getElementById('map'), options);

                // Creating a marker
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(<?php echo $options['art_nouveau_contact_us_block_address']['lat'];?>, <?php echo $options['art_nouveau_contact_us_block_address']['lng'];?>),
                    map: map,
                    title: '<?php echo $options['art_nouveau_contact_us_block']['map_marker_title'];?>'
                });

                // Creating an InfowWindow
                var infowindow = new google.maps.InfoWindow({
                    content: '<?php echo $options['art_nouveau_contact_us_block']['map_window_text'];?>'
                });

                // Adding a click event to the marker
                google.maps.event.addListener(marker, 'click', function() {
                    // Opening the InfoWindow
                    infowindow.open(map, marker);
                });
            }
        })();
    </script>
    <?php endif;?>
</head>
<body>
<div class="wrapper">
    <div class="header">
        <div class="header-bg">

                <div class="fixed-width overflow-hidden" style="width: <?php echo $options['art_nouveau_menu_header_width'];?>px;">
                    <div class="nav font-swaak text-align-center">
                        <ul>
                            <?php foreach($options['art_nouveau_menu_title'] as $val):?>
                                <li><a href="<?php echo get_site_url().$val['link']?>" class="" data-anchor="<?php echo $val['link']?>"><?php echo strtoupper($val['title']);?></a></li>
                            <?php endforeach;?>
                        </ul>
                    </div>
                    <a href="/" class="no-decoration">
                        <div class="header-logo text-align-center">
                            <h1 class="slogan font-swaak"><?php echo strtoupper($options['art_nouveau_header_site_name']);?></h1>
                            <h3 class="sub-slogan"><?php echo $options['art_nouveau_header_site_slogan'];?></h3>
                        </div>
                    </a>
                </div>         

        </div>
    </div>