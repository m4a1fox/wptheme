
(function($) {
    return $.fn.horizontalSlider = function(settings) {
        var arrowTemplate, options, paginationTemplate, wrapperTemplate;
        if (settings == null) {
            settings = {};
        }
        options = $.extend({
            selected: 1,
            opacity: 0.2,
            speed: 1000,
            arrowControl: true
        }, settings);
        arrowTemplate = '<div class="js-hp-paging-arrow hp-paging-arrow"></div>';
        paginationTemplate = "<ul class='js-hp-paging hp-paging'></ul>";
        wrapperTemplate = '<div class="hp"></div>';
        $(this).each(function(index, item) {
            var $wrapper, animating, backArrow$, el$, items$, navigate, nextArrow$, pagination$, _getCurrent, _total;
            animating = false;
            el$ = $(item);
            el$.css('overflow', 'hidden');
            el$.find('>li').css({'display': 'block', 'float': 'left', 'width': '100%'});
            el$.wrapAll(wrapperTemplate);
            $wrapper = el$.parent();
            el$.css({
                'position': 'relative'
            }, 'overflow-y', 'hidden');
            items$ = el$.find(' > li');
            _total = items$.size();
            nextArrow$ = $(arrowTemplate).addClass('js-hp-paging-back hp-paging-next');
            backArrow$ = $(arrowTemplate).addClass('js-hp-paging-back hp-paging-back');
            pagination$ = $(paginationTemplate);
            items$.each(function(index, item) {
                var className;
                if (index + 1 === options.selected) {
                    className = 'selected';
                } else {
                    className = '';
                }
                return pagination$.append("<li class='" + className + "'><a href='#'></a></li>");
            });
            if (options.allowControl) {
                $wrapper.append(backArrow$);
                $wrapper.append(nextArrow$);
            }
            $wrapper.append(pagination$);
            _getCurrent = function() {
                var li$;
                li$ = pagination$.find('li');
                return li$.index(pagination$.find('li.selected'));
            };
            navigate = function(index) {
                var current, leftDiff, newIndex;
                if (index == null) {
                    index = _getCurrent();
                }
                if (animating === false) {
                    current = _getCurrent();
                    if (current === index) {
                        return items$.css({
                            left: -10-el$.width(),
                            position: 'absolute',
                            opacity: options.opacity
                        }).eq(index).css({
                                left: 'auto',
                                position: 'relative'
                            }).animate({
                                opacity: '1'
                            }, options.speed);
                    } else {
                        animating = true;
                        newIndex = index;
                        if (index >= _total) {
                            newIndex = 0;
                        }
                        if (index < 0) {
                            newIndex = _total - 1;
                        }
                        leftDiff = current > index ? 10+el$.width() : -10-el$.width();
                        items$.eq(current).css({
                            position: 'absolute',
                            zIndex: current > index ? 0 : 1
                        }).animate({
                                left: leftDiff,
                                opacity: options.opacity
                            }, options.speed);
                        items$.eq(newIndex).css({
                            position: 'relative',
                            left: 'auto',
                            zIndex: 0
                        }).animate({
                                opacity: 1
                            }, options.speed, function() {
                                return animating = false;
                            });
                        pagination$.find('li').removeClass('selected');
                        return pagination$.find('li').eq(newIndex).addClass('selected');
                    }
                }
            };
            pagination$.find('li a').click(function(event) {
                navigate(pagination$.find('li').index($(event.currentTarget).parent()));
                event.preventDefault();
                return false;
            });
            nextArrow$.click(function(event) {
                navigate(_getCurrent() + 1);
                event.preventDefault();
                return false;
            });
            backArrow$.click(function(event) {
                navigate(_getCurrent() - 1);
                event.preventDefault();
                return false;
            });
            return navigate();
        });
        return this;
    };
})(jQuery);
