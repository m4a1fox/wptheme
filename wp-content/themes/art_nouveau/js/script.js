$(document).ready(function () {

    var url = window.location.href.split('#');

    function getBlogTpl(data, photoAlign) {
        var d = new Date(data.post_date),
            curr_date = d.getDate(),
            curr_month = d.getMonth() + 1,
            curr_year = d.getFullYear(),
            textAlign = photoAlign == 'right' ? 'text-align-left' : 'text-align-right';

        curr_month = curr_month > 10 ? curr_month : '0' + curr_month;
        var postDate = curr_date + "." + curr_month + "." + curr_year;

        return '<div class="whiteBlock clearfix rounded blogItem position-relative post_category_id_right">' +
                '<div class="fluid width-73" style="float: ' + photoAlign + ';">' +
                '<div class="padded">' +
                '<div class="' + textAlign + '">' +
                '<a href="' + data.guid + '"><h2>' + data.post_title + '</h2></a>' +
                '<div class="desc-text">' + postDate + '</div>' +
                '</div>' +
                '<p>' + data.shortText + '</p>' +
                '<br>' +
                '<br>' +
                '<div class="blogControl ' + photoAlign + '">' +
                '<div class="fluid width-50 text-align-left">' + data.category_id_custom + '' +
                '</div>' +
                '<div class="fluid width-50 text-align-right">Comments: '+ data.comment_count +'</div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '<div class="fluid width-25 text-align-right">' +
                '<img src="' + data.image + '" alt="' + data.post_title + '"/>' +
                '</div>' +
                '</div>'
            ;
    }


    $('.js-categories-dd').customDropdown();
    $('.js-carousel').horizontalSlider({speed: 500, arrowControl: false});

    $('li', '.main-page .dropdown-list').click(function () {

        var currentCategoryText = $(this).text(),
            photoAlign = jQuery('.blog-content').data('align'),
            currentCategoryId = $('.js-categories-dd option[data-text="' + currentCategoryText + '"]').val(),
            blogHtml = '',
            data = {action: 'get_post_by_category', categoryId: currentCategoryId, categoryText: currentCategoryText};


        $.ajaxSetup({cache: false});

        $.ajax({
            type: "POST",
            url: "/wp-admin/admin-ajax.php",
            data: data,
            success: function (res) {
                var response = jQuery.parseJSON(res);
                for (var prop in response.postByCategory) {
                    if (response.postByCategory.hasOwnProperty(prop)) {
                        blogHtml += getBlogTpl(response.postByCategory[prop], photoAlign);
                    }
                }

                $('.blog-content').html(blogHtml);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert(errorThrown);
            }
        });
    });

    $('li', '.category-block .dropdown-list').click(function () {
        var currentCategoryText = $(this).text(),
            currentCategoryId = $('.js-categories-dd option[data-text="' + currentCategoryText + '"]').val();

            window.location.href="?cat="+currentCategoryId;
        });


    var placeholder = '',
        commentForm = $('.comment-form'),
        commentsInputsTextArea = $('input[type="text"], textarea', commentForm),
        placeholderText = {
            author: 'Name*',
            email: 'e-mail*',
            comment: 'Your Comment*'
        },
        commentsTextAlignLeft = {'text-align': 'left'};

    if (url[1] == 'respond') {
        commentForm.show();
    }

    var currentCategoryText = $('.category-block').data('currentText');

    if(currentCategoryText != undefined){
        $('li', '.dropdown-list').each(function(){
            if($(this).attr('title') == currentCategoryText){
                $(this).addClass('selected')
            }else{
                $(this).removeClass('selected');
            }
        });

        $('.custom-dropdown .current-label').attr('title', currentCategoryText).text(currentCategoryText);

    }


    commentsInputsTextArea.each(function () {
        var inputId = $(this).attr('id');
        $(this).attr('placeholder', placeholderText[inputId]);
        $(this).focus(function () {
            if ($(this).val().length == 0) {
                $(this).css(commentsTextAlignLeft).attr('placeholder', '');
            }
        });

        $(this).blur(function () {
            if ($(this).val().length == 0) {
                $(this).attr('placeholder', placeholderText[inputId]).removeAttr('style');
            }
        })
    });


    $('a', '.leave-comment-btn').click(function (e) {
        e.preventDefault();
        $('.comment-form').show();
    });

    function scrollToAnchor(aid){
        var aTag = $(".content-block[data-name='"+ aid +"']");
        $('html,body').animate({scrollTop: aTag.offset().top-40},'slow');
    }

    $("li a", ".nav").click(function() {
        var anchor = $(this).data('anchor');

        scrollToAnchor(anchor);
    });

    $('.top-btn').click(function(){
        var html = $(window.opera?'html':'html, body');
        html.animate({scrollTop: 0},'slow');
    });

    $('.portfolio-image').click(function(e){
        e.preventDefault();
        var imageBlock = $('.block-for-image');
        imageBlock.addClass('image-selected');
        var image = $(this).attr('href');
        imageBlock.html('<span></span><div class="image-container"><img src="' + image + '" class="only-image"><a href="#"></a></div>');
    });

    $('.block-for-image, .block-for-image .image-container a').click(function(e){
        e.preventDefault();

        if(e.target.className != 'only-image'){
            $(this).html('');
            $(this).removeClass('image-selected')
        }
    });

    if(document.body.clientWidth <= 1024){
        jQuery('.header-bg > .fixed-width').css({'width': '100%'});
    }

    if(document.body.clientWidth < 1024){
        var iframeParent = jQuery('.art-block-widget-video ');
        var iframeParentHeight = jQuery(iframeParent).height();
        jQuery('iframe', iframeParent).attr({'width': '100%', 'height': iframeParentHeight});
    }

    if(document.body.clientWidth < 768){
        jQuery('.portfolio-images .whiteBlock').hover(function(e){
            $(this).css({'cursor': 'default'});
            e.preventDefault();
            $(this).find('.hoverElement').hide();
        });
    }


});