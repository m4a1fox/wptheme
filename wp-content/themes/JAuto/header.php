<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo('charset');?>" />
    <title><?php echo wp_title('<<', true, 'right');?> | <?php echo bloginfo('name');?></title>
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
</head>
<body>
<div id="page">
    <div id="header">
        <div id="headerimg">
            <h1><a href="<?php echo get_option('home');?>"><?php echo bloginfo('name');?></a> <?php echo get_option('ja_theme_options')['ja_footer_text']?></h1>
            <div class="description"><?php bloginfo('description');?></div>
        </div>
        <ul id="nav">
            <?php wp_list_pages('sort_column=menu_order&depth=1&title_li=');?>
        </ul>
    </div>