<?php get_header(); ?>
<div id="content">
    <?php the_title();?>
    <?php if (have_posts()) { ?>
    <?php while (have_posts()) {the_post();}?>
    <div class="post">
        <?php the_content()?>
        <?php }?>
    </div>
<?php get_sidebar(); ?>