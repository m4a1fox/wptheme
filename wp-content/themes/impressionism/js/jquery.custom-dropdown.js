/*
 *  Replacing integer inputs with a slider. (require jQuery UI)
 *  
 **/

(function($) {
  return $.fn.customDropdown = function(settings) {
    var selectOptionHandler, toggleSelectHandler, writeOptions;
    settings = settings || {enableTitle: true};
    toggleSelectHandler = function(event) {
      var $dropdown, $this;
      $this = $(this);
      $dropdown = $this.closest('.custom-dropdown');
      event.preventDefault();
      $(".open", $dropdown).not($dropdown).removeClass("open");
      $dropdown.toggleClass("open");
      if ($dropdown.hasClass("open")) {
        $(document.body).bind("click.hideSelect", function(event) {
          if ($(event.target).parent('.custom-dropdown').length === 0) {
            $dropdown.removeClass("open");
            return $(document.body).unbind("click.hideSelect");
          }
        });
      } else {
        $(document.body).unbind("click.hideSelect");
      }
      return false;
    };
    selectOptionHandler = function(event) {
      var $customDropdown, $select, $this, selectedIndex;
      $this = $(this);
      $customDropdown = $this.closest('.custom-dropdown');
      $select = $customDropdown.prev();
      selectedIndex = 0;
      $this.parents('.custom-dropdown > ul').find('li').removeClass('selected');
      $this.addClass('selected');
      $customDropdown.removeClass('open').find('.current-label').html($this.html());
      if (settings.enableTitle){
          $customDropdown.find('.current-label').attr('title', $this.html());
      }
      $this.parents('.custom-dropdown > ul').find('li').each(function(index) {
        if ($this[0] === this) {
          return selectedIndex = index;
        }
      });
      $select[0].selectedIndex = selectedIndex;
      $($select[0]).trigger('change');
      return false;
    };
    writeOptions = function($opt, $parent, name) {
      $opt.each(function() {
        var $li, titleHtml;
        titleHtml = '';
        if (settings.enableTitle){
            titleHtml = ' title="' + $(this).html() + '"';
        }
        $li = $('<li class="option"' + titleHtml + '>' + $(this).html() + '</li>');
        if (settings.liClass) {
          $li.addClass(settings.liClass);
        }
        if (settings.defaultLabel && settings.defaultLabel === $(this).html()) {
          $li.addClass('default-label');
        }
        return $parent.append($li);
      });
      if (name) {
        return $parent.prepend('<div class="optgroup-label">' + name + '</div>');
      }
    };
    $(this).each(function() {
      var $customSelect, $optgroups, $options, $optionsWithoutGroups, $selected, $this, titleHtml;
      $this = $(this);
      $customSelect = $this.next('.custom-dropdown');
      $options = $this.find('option');
      $optgroups = $this.find('optgroup');
      $optionsWithoutGroups = $this.find('> option');
      if ($customSelect.length === 0) {
        $customSelect = $('<div class="custom-dropdown"><ul class="dropdown-list"></ul></div>"');
        if (settings.className) {
          $customSelect.addClass(settings.className);
        }
        writeOptions($optionsWithoutGroups, $customSelect.find('ul'));
        if ($optgroups.length > 0) {
          $optgroups.each(function() {
            var $childOptions, $optGroupUl;
            $optGroupUl = $('<ul class="optgroup"></ul>');
            $customSelect.find('>ul').append($optGroupUl);
            $childOptions = $(this).find('option');
            return writeOptions($childOptions, $optGroupUl, $(this).attr('label'));
          });
        }
        titleHtml = '';
        if (settings.enableTitle){
            titleHtml = ' title="' + ($options.first().html() ? $options.first().html() : '') + '"';
        }
        $customSelect.prepend('<a href="#" class="current fake-input">' + 
                      '<div class="current-label"' + titleHtml + '>' + 
                      ($options.first().html() ? $options.first().html() : '') +
                      '</div>' +
                      '<span class="custom-dropdown-arrow"></span></a>');
        $this.after($customSelect);
        $this.hide();
      }
      $customSelect.width($this.innerWidth()*1 - 35);
//      debugger;
      $selected = $('option:selected', this);
      if ($selected.length === 1) {
        $customSelect.find('li').removeClass('selected');
        $customSelect.find("li:contains('" + ($selected.text()) + "')").addClass('selected');
        $customSelect.find('.current-label').html($selected.html());
        if (settings.enableTitle){
            $customSelect.find('.current-label').attr('title', $selected.text());
        }
      } else if (settings.defaultLabel) {
        $customSelect.find('.current-label').html(settings.defaultLabel ? settings.defaultLabel: '');
      }
      $customSelect.find('li').addClass('styled');
      $('a', $customSelect).click(toggleSelectHandler);
      return $('li', $customSelect).click(selectOptionHandler);
    });
    return this;
  };
})(jQuery);
