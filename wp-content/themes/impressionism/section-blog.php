<?php $options = get_option('impressionism_theme_options_menu');?>
<div class="fixed-width divider rounded">
    <div class="top-btn">
        TOP
    </div>
</div>
<div class="content-block blogBlock main-page" data-name="#blog">
    <div class="fixed-width position-relative">

        <h1 class="font-mucha font-size-60 darkBlue font-weight-normal margin-bottom">
            <?php echo $options['impressionism_blog_block']['title']?>
        </h1>

        <div class="position-absolute blogCategories">
            <select class="js-categories-dd">
                <option value="0" data-text="Categories">Categories</option>
                <?php foreach (get_categories() as $key => $value): ?>
                    <?php if($value->term_id != 1):?>
                        <option value="<?php echo $value->term_id; ?>"
                                data-text="<?php echo $value->cat_name ?>"><?php echo $value->cat_name; ?></option>
                    <?php endif;?>
                <?php endforeach; ?>

            </select>
        </div>
        <div class="blog-content" data-align="<?php echo $options['impressionism_blog_block']['photo_float'] == 'right' ? 'right' : 'left'; ?>">
            <?php if (have_posts()) { ?>
                <?php while (have_posts()) {
                    the_post(); ?>
                    <?php $content = catch_that_image(get_the_content('more'));?>
                    <div class="whiteBlock clearfix rounded blogItem position-relative post_category_id_<?php echo $categoryId[0]; ?>">
                        <?php if(!isset($options['impressionism_blog_block']['default_image']) && $content['default'] == 1):?>
                            <div class="fluid width-73" style="<?php echo $options['impressionism_blog_block']['photo_float'] == 'right' ? 'float: right;' : ''; ?> width: 100%;" >
                        <?php else:?>
                            <div class="fluid width-73" style="<?php echo $options['impressionism_blog_block']['photo_float'] == 'right' ? 'float: right;' : ''; ?>">
                        <?php endif;?>

                            <div class="padded">
                                <div class="<?php echo $options['impressionism_blog_block']['photo_float'] == 'right' ? 'text-align-left' : 'text-align-right'; ?>">
                                    <a href="<?php the_permalink() ?>"><h2><?php the_title(); ?></h2></a>
                                    <div class="desc-text"><?php the_time('d.m.Y') ?></div>
                                </div>
                                <p><?php echo $content['text'] ?></p>
                                <br>
                                <br>
                                <?php if(!isset($options['impressionism_blog_block']['default_image']) && $content['default'] == 1):?>
                                    <div class="blogControl <?php echo $options['impressionism_blog_block']['photo_float'] == 'right' ? 'right' : ''; ?>" style="width: 97%;">
                                        <div class="fluid width-50 text-align-left"><?php echo getCategoryPostList(get_the_ID()); ?>
                                        </div>
                                        <div class="fluid width-50 text-align-right">Comments: <?php echo wp_count_comments(get_the_ID())->total_comments; ?></div>
                                    </div>
                                <?php else:?>
                                    <div class="blogControl <?php echo $options['impressionism_blog_block']['photo_float'] == 'right' ? 'right' : ''; ?>">
                                        <div class="fluid width-50 text-align-left"><?php echo getCategoryPostList(get_the_ID()); ?>
                                        </div>
                                        <div class="fluid width-50 text-align-right">Comments: <?php echo wp_count_comments(get_the_ID())->total_comments; ?></div>
                                    </div>
                                <?php endif;?>

                            </div>
                        </div>
                        <?php if(!isset($options['impressionism_blog_block']['default_image']) && $content['default'] == 1):?>
<!--                            <div class="fluid width-25 text-align-right">-->
<!--                                <img src="--><?php //echo $content['image'] ?><!--" alt="--><?php //the_title(); ?><!--"/>-->
<!--                            </div>-->
                        <?php else:?>
                            <div class="fluid width-25 text-align-right">
                                <img src="<?php echo $content['image'] ?>" alt="<?php the_title(); ?>"/>
                            </div>
                        <?php endif;?>
                    </div>

                <?php } ?>
            <?php } ?>
            <?php impressionism_content_nav( 'nav-below' ); ?>
        </div>
        <div class="block-widgets js-block-widgets"><?php get_sidebar();?></div>
    </div>
</div>

