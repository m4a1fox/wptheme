jQuery(document).ready(function () {

    function countImagesBlock(block) {
        var col = jQuery("> div:last", block).data("id");
        if (col == undefined) {col = 0;}

        return ++col;
    }

    function imageBlock(col_inputs, whichBlock) {
        var blocks = [];

        blocks['slideBlockImage'] = '' +
            '<div data-id="' + col_inputs + '" class="js-image-parent-block image-parent-block">' +
            '<span class="js-delete-button delete-button"></span>' +
            '<div class="media-block">' +
            '<div class="gallery-text text">Picture</div>' +
            '<div class="gallery-input input">' +
            '<a class="button choose-from-library-link"  href="#" data-update-link="' + col_inputs + '">Open Media Library</a>' +
            '<div class="select-image-description">Choose your image, then click "Select" to apply it.</div>' +
            '<input class="imgUrl" style="display: none;" id="inp' + col_inputs + '" type="text" name="impressionism_images[' + col_inputs + '][url]" value="" />' +
            '</div>' +
            '</div>' +
            '<div class="link-block">' +
            '<div class="link-text text">Link</div>' +
            '<div class="link-input input"><input class="link" id="inp' + col_inputs + '" type="text" name="impressionism_images[' + col_inputs + '][link]" value="" placeholder="http://" /></div>' +
            '</div>' +
            '<div class="caption-block">' +
            '<div class="caption-text text">Caption</div>' +
            '<div class="caption-input input"><input id="inp' + col_inputs + '" type="text" name="impressionism_images[' + col_inputs + '][caption]" value="" /></div>' +
            '</div>' +
            '<div class="description-block">' +
            '<div class="description-text text">Description</div>' +
            '<div class="description-input input"><textarea name="impressionism_images[' + col_inputs + '][description]"></textarea></div>' +
            '</div>' +
            '<div class="sortable-handler"></div>' +
            '</div>'
        ;

        blocks['portfolioBlockImage'] = '' +
            '<div data-id="' + col_inputs + '" class="image-parent-block">' +
            '<span class="js-delete-button delete-button"></span>' +
            '<div class="media-block">' +
            '<div class="media-text text">Picture</div>' +
            '<div class="media-input input">' +
            '<a class="button choose-from-library-link"  href="#" data-update-link="' + col_inputs + '">Open Media Library</a> ' +
            '<span class="js-create-thumb-process-image create-thumb-process-image"></span>' +
            '<div class="select-image-description">Choose your image, then click "Select" to apply it.</div>' +
            '<input class="imgUrl" style="display: none;" id="inp' + col_inputs + '" type="text" name="impressionism_portfolio[' + col_inputs + '][url]" value="" />' +
            '<input class="imgUrlThumb" style="display: none;" id="inp' + col_inputs + '" type="text" name="impressionism_portfolio[' + col_inputs + '][thumb]" value="" />' +
            '</div>' +
            '</div>' +
            '<div class="title-block">' +
            '<div class="title-text text">Title</div>' +
            '<div class="title-input input"><input class="link" id="inp' + col_inputs + '" type="text" name="impressionism_portfolio[' + col_inputs + '][title]" value="" /></div>' +
            '</div>' +
            '<div class="sortable-handler"></div>' +
            '</div>'
        ;

        blocks['aboutBlockContent'] = '' +
            '<div data-id="' + col_inputs + '" class="js-image-parent-block image-parent-block">' +
            '<span class="js-delete-button delete-button"></span>' +
            '<div class="media-block">' +
            '<div class="gallery-text text">Picture</div>' +
            '<div class="gallery-input input">' +
            '<a class="button choose-from-library-link"  href="#" data-update-link="' + col_inputs + '">Open Media Library</a>' +
            '<div class="select-image-description">Choose your image, then click "Select" to apply it.</div>' +
            '<input class="imgUrl" style="display: none;" id="inp' + col_inputs + '" type="text" name="impressionism_about[' + col_inputs + '][url]" value="" />' +
            '</div>' +
            '</div>' +
            '<div class="link-block">' +
            '<div class="link-text text">Name</div>' +
            '<div class="link-input input"><input class="link" id="inp' + col_inputs + '" type="text" name="impressionism_about[' + col_inputs + '][name]" value="" /></div>' +
            '</div>' +
            '<div class="caption-block">' +
            '<div class="caption-text text">Status</div>' +
            '<div class="caption-input input"><input id="inp' + col_inputs + '" type="text" name="impressionism_about[' + col_inputs + '][status]" value="" /></div>' +
            '</div>' +
            '<div class="description-block">' +
            '<div class="description-text text">Description</div>' +
            '<div class="description-input input"><textarea name="impressionism_about[' + col_inputs + '][description]"></textarea></div>' +
            '</div>' +
            '<div class="sortable-handler"></div>' +
            '</div>'
        ;

        blocks['socialBlockContent'] = '' +
            '<div data-id="'+ col_inputs +'" class="image-parent-block">' +
            '<span class="js-delete-button delete-button"></span>' +
            '<div class="media-block">' +
            '<div class="media-text text">Picture</div>' +
            '<div class="media-input input">' +
            '<a class="button choose-from-library-link"  href="#" data-update-link="'+ col_inputs +'">Open Media Library</a>' +
            '<span class="js-create-thumb-process-image create-thumb-process-image"></span>' +
            '<div class="select-image-description">Choose your image, then click "Select" to apply it.</div>' +
            '<input class="imgUrl" style="display: none;" id="inp'+ col_inputs +'" type="text" name="impressionism_social_network['+ col_inputs +'][url]" value="" />' +
            '</div>' +
            '</div>' +
            '<div class="title-block">' +
            '<div class="title-text text">Link</div>' +
            '<div class="title-input input"><input class="link" id="inp'+ col_inputs +'" type="text" name="impressionism_social_network['+ col_inputs +'][title]" value="" placeholder="http://" /></div>' +
            '</div>' +
            '<div class="sortable-handler"></div>' +
        '</div>'
        ;

        return blocks[whichBlock];
    }


    var i = 0;

    function saveSettingCounter() {
        var counterSpan = jQuery('#impressionism-save-settings-counter');
        ++i;
        if (i > 0) {

            counterSpan.show();
            counterSpan.text(i);
        }
    }

    jQuery("form :input").change(function () {
        saveSettingCounter();
    });

    jQuery('.general-settings #impressionism-save-settings').click(function () {
        if(!jQuery(this).hasClass('disabled')){
            jQuery('.general-settings #impressionism-form-settings').submit();
        }
    });

    jQuery(function () {
        var amount = jQuery("#amount"),
            sliderBlock = jQuery("#slider-range-min");
        var headerWidth = amount.val();
        sliderBlock.slider({
            range: "min",
            value: headerWidth,
            min: 960,
            max: 1200,
            slide: function (event, ui) {
                jQuery("#amount").val(ui.value);
            },
            change: function () {
                saveSettingCounter();
            }
        });
        amount.val(sliderBlock.slider("value"));
    });

    jQuery(function () {
        var sortAbleList = jQuery("#sortable");
        sortAbleList.sortable({update: function () {
            saveSettingCounter();
        }});
        sortAbleList.disableSelection();
    });

    jQuery(function () {
        var sortAbleList = jQuery("#div_inputs_social, #div_inputs_about, #div_inputs_portfolio, #div_inputs");
        sortAbleList.sortable({
            opacity: .3,
            cursor: "move",
            axis: "y",
            handle: ".sortable-handler",
            update: function () {
            saveSettingCounter();
        }});
        sortAbleList.disableSelection();


    });


    jQuery("#add-new-slide").on('click', function () {
        var block = imageBlock(countImagesBlock(jQuery(".js-slideshow #div_inputs")), 'slideBlockImage');
        jQuery(block).appendTo(jQuery("#div_inputs"));
    });

    jQuery("#div_inputs").on('click', ".js-delete-button", function () {
        jQuery(this).parent().remove();
    });

    jQuery("#add-new-image-portfolio").on('click', function () {
        var block = imageBlock(countImagesBlock(jQuery(".js-portfolio #div_inputs_portfolio")), 'portfolioBlockImage');
        jQuery(block).appendTo(jQuery("#div_inputs_portfolio"));
    });

    jQuery("#div_inputs_portfolio").on('click', ".js-delete-button", function () {
        jQuery(this).parent().remove();
    });

    jQuery("#add-new-block-about").on('click', function () {
        var block = imageBlock(countImagesBlock(jQuery(".js-about #div_inputs_about")), 'aboutBlockContent');
        jQuery(block).appendTo(jQuery("#div_inputs_about"));
    });

    jQuery("#div_inputs_about").on('click', ".js-delete-button", function () {
        jQuery(this).parent().remove();
    });

    jQuery("#add-new-social-network").on('click', function () {
        var block = imageBlock(countImagesBlock(jQuery(".js-social #div_inputs_social")), 'socialBlockContent');
        jQuery(block).appendTo(jQuery("#div_inputs_social"));
    });

    jQuery("#div_inputs_social").on('click', ".js-delete-button", function () {
        jQuery(this).parent().remove();
    });


    var $sidebar   = jQuery(".admin-menu"),
        $window    = jQuery(window),
        offset     = $sidebar.offset(),
        topPadding = 15;

    $window.scroll(function() {
        if ($window.scrollTop() > offset.top) {
            $sidebar.stop().animate({
                marginTop: $window.scrollTop() - offset.top + topPadding

            }, 100);
        } else {
            $sidebar.stop().animate({
                marginTop: 0
            }, 100);
        }
    });

    jQuery('.info').click(function(){
        jQuery('.docs').show();
    });

    jQuery('body').click(function(e){
        if(e.target.className != 'info'){
            jQuery('.docs').hide();
        }
    });
});