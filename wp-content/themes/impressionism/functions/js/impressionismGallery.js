/**
 * Gallery. Start Code.
 */
var frame;
(function ($) {
    function getImageUrl(event, buttonLink, parentBlock, createThumb){
        createThumb = !!(createThumb !== undefined);



        var $el = $(buttonLink);
        parentBlock.data('lastClicked', $el.data('updateLink'));
        event.preventDefault();

        if (frame) {
            frame.open();
            return;
        }

        frame = wp.media.frames.customHeader = wp.media({
            title: $el.data('choose'),
            library: {
                type: 'image'
            },
            button: {
                text: $el.data('update'),
                close: false
            }
        });

        frame.on('select', function () {

            var attachment = frame.state().get('selection').first(),
                imgUrl = attachment.attributes.url,
                inputId = parentBlock.data('lastClicked'),
                inputField = $("div[data-id='"+inputId+"']", parentBlock);
            inputField.find('input.imgUrl').val(imgUrl);
            inputField.find('input.imgUrl').show();
            console.log(parentBlock)

            if(createThumb){
                $('#impressionism-save-settings').addClass('disabled');
                inputField.find('.js-create-thumb-process-image').css({'display': 'inline-block'});
                var data = {action: 'save_thumb', url: imgUrl};
                $.ajax({
                    type: "POST",
                    url: "/wp-admin/admin-ajax.php",
                    data: data,
                    success: function(data){
                        inputField.find('input.imgUrlThumb').val(data);
                        inputField.find('.js-create-thumb-process-image').removeAttr('style');
                        $('#impressionism-save-settings').removeClass('disabled');
                    }
                });
            }

            frame.close();
        });
        frame.open();
    }

    $(function () {
        $('.js-slideshow').on('click', '.choose-from-library-link', function (event) {
            getImageUrl(event, this, $('.js-slideshow #div_inputs'));
        });

        $('.js-portfolio').on('click', '.choose-from-library-link', function (event) {
            getImageUrl(event, this, $('.js-portfolio #div_inputs_portfolio'), true);
        });

        $('.js-about').on('click', '.choose-from-library-link', function (event) {
            getImageUrl(event, this, $('.js-about #div_inputs_about'));
        });

        $('.js-our-team').on('click', '.choose-from-library-link', function (event) {
            getImageUrl(event, this, $('.js-our-team #div_inputs_our_team'));
        });

        $('.js-social').on('click', '.choose-from-library-link', function (event) {
            getImageUrl(event, this, $('.js-social #div_inputs_social'));
        });
    });
}(jQuery));

/**
 * Gallery End Code.
 */