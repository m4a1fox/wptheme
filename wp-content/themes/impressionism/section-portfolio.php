<?php $options = get_option('impressionism_theme_options_menu');?>
<div class="fixed-width divider rounded">
    <div class="top-btn">
        TOP
    </div>
</div>
<div class="content-block portfolioBlock" data-name="#portfolio">
    <div class="fixed-width">
        <h1 class="font-mucha font-size-60 pink font-weight-normal margin-bottom"><?php echo $options['impressionism_portfolio_block_title'];?></h1>
        <div class="unmargin-l clearfix portfolio-images">
            <?php if($options['impressionism_portfolio_image']):?>
                <?php foreach($options['impressionism_portfolio_image'] as $key=> $val):?>
                    <a href="<?php echo $val['url']?>" class="portfolio-image">
                        <div class="fluid width-33">
                            <div class="whiteBlock margin-l rounded">
                                <img src="<?php echo $val['thumb']?>" alt="<?php echo $val['title']?>"/>
                                <div class="portfolioDesc padded">
                                    <?php echo $val['title']?>
                                </div>
                                <div class="hoverElement position-absolute"></div>
                            </div>
                        </div>
                    </a>
                <?php endforeach;?>
            <?php endif;?>
        </div>
    </div>
</div>