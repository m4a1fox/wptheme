<?php get_header(); ?>
<?php $options = get_option('impressionism_theme_options_menu'); ?>
<div class="fixed-width">

    <h2 class="font-swaak"><?php echo $options['impressionism_blog_block']['title'] ?></h2>
    <?php if (have_posts()) { ?>
    <?php while (have_posts()) {
        the_post(); ?>
        <?php $content = catch_that_image(get_the_content());?>
        <div class="post blog-post">

            <div class="entry">
                <?php if(!isset($options['impressionism_blog_block']['default_image']) && $content['default'] == 1):?>

                <?php else:?>
                    <img src="<?php echo $content['image'] ?>" alt="" align="left" style="margin-bottom: 5px;"/>
                <?php endif;?>


                <div class="post-title">
                    <h1><?php the_title(); ?></h1>

                    <div class="post-date"><span class="post-month"><?php the_time('d.m.Y') ?></span></div>
                </div>
                <?php echo $content['text']; ?>
                <div
                    class="blogControl <?php echo $options['impressionism_blog_block']['photo_float'] == 'right' ? 'right' : ''; ?>">
                    <div class="fluid width-50 text-align-left">
                        <p><?php echo getCategoryPostList(get_the_ID()); ?></p>
                    </div>
                    <div class="fluid width-50 text-align-right"><p>
                            Comments: <?php echo wp_count_comments(get_the_ID())->total_comments; ?></p></div>
                </div>
                <?php wp_link_pages(array('before' => '<p><strong>Pages:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?><!-- ??????? ?? index.php -->
            </div>
            <br>

            <div class="navigation single-navigation">
                <span class="previous-entries"><?php previous_post_link(' %link') ?></span>
                <span class="next-entries"><?php next_post_link('%link') ?></span>
            </div>
            <div class="comments-area">
                <?php comments_template( '', true, 'comments' ); ?>
            </div>

            <div class="leave-comment-btn">
                <a href="#">Leave a comment</a>
            </div>

            <div class="comment-form">
                <?php comment_form(); ?>
            </div>

            <?php } ?>


        </div>

        <div class="block-widgets js-block-widgets"><?php get_sidebar(); ?></div>
    <?php } ?>


</div>
<div class="secondary-page contact rounded"></div>
<div class="content-block contactBlock contact-secondary-page">
<?php get_footer();?>
