<?php $options = get_option('impressionism_theme_options_menu'); ?>
<div class="fixed-width">
    <h1 class="font-mucha font-size-60 darkBlue font-weight-normal margin-bottom"><?php echo $options['impressionism_contact_us_block']['title'];?></h1>
    <div class="clearfix">
        <div class="fluid width-50">
            <?php
            echo $options['impressionism_contact_us_block']['address'];
            echo $options['impressionism_contact_us_block']['phone'];
            echo $options['impressionism_contact_us_block']['email'];
            ?>
        </div>
        <div class="fluid width-50 js-google-map">
            <?php if($options['impressionism_contact_us_block']['show_map']):?>
                <div id="map" style="min-width: 320px; height: 200px;"></div>
            <?php endif;?>
        </div>
    </div>
</div>
</div>
<?php $i = 0; foreach($options['impressionism_social_network_custom'] as $k=>$v){if(!empty($v)){$i++;}}?>
<?php if($options['impressionism_social_network']){ $k = 0; foreach($options['impressionism_social_network'] as $key=>$value){if(!empty($value)){$k++;}}}?>
<?php if($i != 0 || $k != 0):?>
<div class="content-block followBlock text-align-center">
    <div class="fixed-width">
        <h2 class="font-weight-normal">Follow us:</h2>
        <div class="clearfix">
            <ul class="followItems clear-ul vertical-list clearfix">
                <?php if($options['impressionism_social_network_custom']):?>
                    <?php foreach($options['impressionism_social_network_custom'] as $key=>$value):?>
                        <?php if(!empty($value)):?>
                            <li><a href="<?php echo $value?>" class="<?php echo $key?>"></a></li>
                        <?php endif;?>
                    <?php endforeach;?>
                <?php endif;?>
                <?php if($options['impressionism_social_network']):?>
                    <?php foreach($options['impressionism_social_network'] as $key=>$value):?>
                        <li><a href="<?php echo $value['link']?>"><img src="<?php echo $value['url']?>" alt="<?php echo $value['link']?>" width="90px" height="90px"/></a></li>
                    <?php endforeach;?>
                <?php endif;?>
            </ul>
        </div>
    </div>
</div>
<?php endif;?>
</div>

</div>
</div>
<div class="block-for-image"></div>
</body>
</html>