<?php $options = get_option('impressionism_theme_options_menu');

?>
<div class="content-block aboutBlock" data-name="#about">
    <div class="fixed-width">
        <h1 class="font-mucha font-size-60 darkBlue font-weight-normal margin-bottom"><?php echo $options['impressionism_about_block_title']?>></h1>
        <ul class="clear-ul clearfix js-carousel">

            <?php if($options['art_about_block_content']):?>

                <?php $count = 1; ?>
                <?php foreach($options['art_about_block_content'] as $key=>$value):?>
                    <?php if ($count == 1) {?>
                    <li>
                    <div class="unmargin-l clearfix">
                    <?php } ?>
                        <div class="fluid width-33">
                            <div class="whiteBlock rounded margin-l height">
                                <img src="<?php echo $value['url']?>" alt="<?php echo $value['name']?>"/>
                                <div class="padded">
                                    <div class="text-align-center">
                                        <h2>
                                            <?php echo $value['name']?>
                                        </h2>
                                        <p>
                                            <?php echo $value['status']?>
                                        </p>
                                    </div>
                                    <div class="text-wrapper">
                                        <?php echo $value['description']?>
                                    </div>
                                </div>
                            </div>
                        </div>
                <?php if ($count == 3) {?>
                    </div>
                    </li>
                <?php $count = 1; } else { $count++; } ?>
                <?php endforeach;?>
            <?php else:?>
                <li>
                <div class="fluid width-33">
                    <div class="whiteBlock rounded margin-l height">
                        <img src="<?php echo get_template_directory_uri();?>/img/about-1.png" alt="about 1"/>
                        <div class="padded">
                            <div class="text-align-center">
                                <h2>
                                    Lorem ipsum
                                </h2>
                                <p>
                                    Lorem ipsum
                                </p>
                            </div>
                            <div class="text-wrapper">
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, adipisci animi consequuntur delectus ex expedita ipsa laudantium maxime molestiae nobis officia praesentium recusandae reiciendis sed sit ut vel voluptatem! Illo.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="fluid width-33">
                    <div class="whiteBlock rounded margin-l height">
                        <img src="<?php echo get_template_directory_uri();?>/img/about-2.png" alt="about 1"/>
                        <div class="padded">
                            <div class="text-align-center">
                                <h2>
                                    Lorem ipsum
                                </h2>
                                <p>
                                    Lorem ipsum
                                </p>
                            </div>
                            <div class="text-wrapper">
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, adipisci animi consequuntur delectus ex expedita ipsa laudantium maxime molestiae nobis officia praesentium recusandae reiciendis sed sit ut vel voluptatem! Illo.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="fluid width-33">
                    <div class="whiteBlock rounded margin-l height">
                        <img src="<?php echo get_template_directory_uri();?>/img/about-3.png" alt="about 1"/>
                        <div class="padded">
                            <div class="text-align-center">
                                <h2>
                                    Lorem ipsum
                                </h2>
                                <p>
                                    Lorem ipsum
                                </p>
                            </div>
                            <div class="text-wrapper">
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam consectetur eius error fugit incidunt ipsa labore nihil recusandae ullam vitae. Atque autem et impedit nobis numquam praesentium repellat rerum suscipit!
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                </li>
            <?php endif;?>
        </ul>
<!--        <div class="position-relative controlBlockWrapper">
            <div class="controlBlock position-absolute">
                <div class="fluid width-33"><div class="control"></div></div>
                <div class="fluid width-33"><div class="control"></div></div>
                <div class="fluid width-33"><div class="control"></div></div>
            </div>
        </div>-->
    </div>
</div>
<div class="content-block teamBlock">
    <div class="fixed-width">
        <div class="whiteBlock clearfix rounded">
            <div class="fluid width-35" style="float: <?php echo $options['impressionism_our_team_block_title']['float'] ? : 'left' ;?>;">
                <img src="<?php echo $options['impressionism_our_team_block_title']['url'];?>" alt="<?php echo $options['impressionism_our_team_block_title']['title'];?>" />
            </div>
            <div class="fluid width-65">
                <div class="padded">
                    <div class="text-align-center">
                        <h2 class="margin-bottom"><?php echo $options['impressionism_our_team_block_title']['title'];?></h2>
                    </div>
                    <?php echo $options['impressionism_our_team_block_title']['description'];?>
                </div>
            </div>
        </div>
    </div>
</div>